package g30125.ursan.stefan.l5.e1;

public class Circle extends Shape{
	double radius;
	public Circle() {
	}
	
	public Circle(double radius) {
		this.radius=radius;
	}
	public Circle(double radius,String color,boolean filled) {
		super(color,filled);
		this.radius=radius;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius=radius;
	}
	public double getArea() {
		return Math.PI*(radius*radius);
	}
	public double getPerimeter() {
		return 2*Math.PI*radius;
	}
	@Override
	   public String toString() {
	       return "Circle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               ", radius=" + radius +
	               '}';
	   }
	
}
