package g30125.ursan.stefan.l5.e2;

public class ProxyImage implements Image {
	 private RealImage realImage;
	 private RotatedImage rotatedimage ;
	   private String fileName;
	 
	   public ProxyImage(String fileName){
	   
	   		if(fileName=="Real")
	   		{
	   		 realImage = new RealImage(fileName);
	   		 realImage.display();
	   		}
	   		else
	   		{
	   			if(fileName=="Rotated")
	   			{
	   			 rotatedimage = new RotatedImage(fileName);
		   		 rotatedimage.display();
	   			}
	   		}
	   }
		     
		   
	   @Override
	   public void display() {
	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      realImage.display();
	   }
}
