package g30125.ursan.stefan.l5.e3;


abstract  class Sensor {
private String location;
abstract public int readValue();
public String getLocation() {
	return location;
}
}

