package g30125.ursan.stefan.l8.e2;

import java.io.*;
import java.util.Scanner;

public class Counter {
	
	 public static void main(String[] args)
		        throws IOException {
		 @SuppressWarnings("resource")
		Scanner reader = new Scanner(System.in);
		 System.out.println("character to count:");
		 char ch = reader.next().charAt(0);
		 int k=0;
		 try {
	            
				DataInputStream in = new DataInputStream(
	              new BufferedInputStream(
	                new FileInputStream("Data.txt")));
				int c;
	             while ((c = in.read()) != -1) {
	            	 if((char)c==ch)
	            	 {
	            		 k++;
	            	 }
	            	 
	             }
	            in.close();
	            System.out.println(k);
	          } catch(EOFException e) {
	            throw new RuntimeException(e);
	          }
	 }
}