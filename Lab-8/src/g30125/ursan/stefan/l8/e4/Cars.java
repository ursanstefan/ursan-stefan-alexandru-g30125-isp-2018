package g30125.ursan.stefan.l8.e4;


import java.io.*;
 
public class Cars {
      public static void main(String[] args) throws Exception{
            CarFactory f = new CarFactory();
 
            Car a = f.createCar("Lamborghini",100000);
            Car b = f.createCar("Ferrari",150000);
 
            f.saveCar(a,"car1.dat");
            f.saveCar(b,"car2.dat");
 
            @SuppressWarnings("unused")
			Car x = f.takeCar("car1.dat");
            @SuppressWarnings("unused")
			Car y = f.takeCar("car2.dat");
      }

}//.class
 
class CarFactory{
	Car createCar(String model,int price){
		Car z = new Car(model,price);
            System.out.println(z+" Car is created.");
            return z;
      }
 
      void saveCar(Car a, String storeRecipientName) throws IOException{
            @SuppressWarnings("resource")
			ObjectOutputStream out =
              new ObjectOutputStream(
                new FileOutputStream(storeRecipientName));
 
            out.writeObject(a);
            System.out.println(a+" Car is saved.");
      }
 
      Car takeCar(String storeRecipientName) throws IOException, ClassNotFoundException{
             @SuppressWarnings("resource")
			ObjectInputStream in =
                    new ObjectInputStream(
                      new FileInputStream(storeRecipientName));
             Car x = (Car)in.readObject();
             System.out.println("Car: Model:"+x.model+", price "+x.price);
             return x;
      }

      
}

 
@SuppressWarnings("serial")
class Car implements Serializable{
	   
	String model;
      int price;
 
      public Car(String model,int price) {
            this.model = model;
            this.price=price;
      }
      public String toString(){return "[car: "+model+", price: "+price+"]";}
      
    
      }
      
