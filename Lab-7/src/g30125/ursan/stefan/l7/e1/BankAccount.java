package g30125.ursan.stefan.l7.e1;

public class BankAccount
{
   private double balance;

   public void withdraw(double amount) {
       this.balance=this.balance-amount;
   }
   public void deposit(double amount) {
       this.balance=this.balance+amount;
   }
   public static void main(String args[]) {
       BankAccountObjects x = new BankAccountObjects("Ursan",12312);
       BankAccountObjects y = new BankAccountObjects("Ursan",12312);
       if(x.equals(y)) System.out.println("Yes"); else System.out.println("No");
   }
}
class BankAccountObjects {
   private String owner;
   public BankAccountObjects(String owner, int balance) {
       this.owner = owner;
   }
   @Override
   public boolean equals(Object obj) {
       if (!(obj instanceof BankAccountObjects))
           return false;
       BankAccountObjects x = (BankAccountObjects) obj;
       return owner.equals(x.owner);
   }

   public int hashCode() {
       return (int) (owner.length() * 1000);
   }
}