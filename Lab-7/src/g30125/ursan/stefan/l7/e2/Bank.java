package g30125.ursan.stefan.l7.e2;

import java.util.*;

public class Bank extends BankAccount{

    private static List<BankAccount> accounts = new ArrayList<BankAccount>();

    public static void getAllAcounts()
    {
        Collections.sort(accounts, new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount arg0, BankAccount arg1) {
                String s0=arg0.getOwner();
                String s1=arg1.getOwner();
                return s0.compareToIgnoreCase(s1);
            }
        });
        for(Object o:accounts){
            System.out.println(o.toString());
        }
    }

    public static void addAcount(String owner,double balance)
    {
        BankAccount p1 = new BankAccount(owner,balance);
        accounts.add(p1);
    }

    public static void printAccounts(double minBalance, double maxBalance)
    {
        System.out.println("\nAccounts between "+minBalance+" and "+maxBalance+" are:");
        for(int i=0;i<accounts.size();i++)
        {
            BankAccount a = (BankAccount) accounts.get(i);
            if(a.getBalance()>minBalance && a.getBalance()<maxBalance)
            {
                System.out.println(a.toString());
            }
        }
    }

    public static void printAccounts()
    {
        Collections.sort(accounts, new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount arg0, BankAccount arg1) {
                if(arg0.getBalance()>arg1.getBalance()) return 1;
                if(arg0.getBalance()==arg1.getBalance()) return 0;
                return -1;
            }
        });

        for(Object o:accounts){
            System.out.println(o.toString());
        }
    }

    public static BankAccount getAccount(String owner)
    {
        for(int i=0;i<accounts.size();i++)
        {
            BankAccount a = (BankAccount) accounts.get(i);
            if (a.getOwner().equals(owner))
            {
                System.out.println("\nAccount found!");
                return a;
            }
        }
        System.out.println("\nAccount with name: "+owner+", was not found!");
        return null;
    }
    public static void main(String[] args)
    {
        addAcount("Adrian",18931);
        addAcount("Florin",8734);
        addAcount("Nicu",55);

        printAccounts();
        printAccounts(10,100);
        System.out.println("\n");
        getAllAcounts();
        System.out.println(getAccount("Florin"));
        System.out.println(getAccount("Marian"));
    }

}

