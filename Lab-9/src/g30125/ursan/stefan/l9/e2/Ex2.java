package g30125.ursan.stefan.l9.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.util.*;

public class Ex2 extends JFrame{

    HashMap accounts = new HashMap();

    //JLabel user,pwd;
    JTextArea tArea;
    JButton bLoghin;

    Ex2(){

        setTitle("Test Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init(){

        this.setLayout(null);
        int width=80;int height = 20;


        bLoghin = new JButton("Increase");
        bLoghin.setBounds(10,50,90, height);

        bLoghin.addActionListener(new TratareButonLoghin());

        tArea = new JTextArea();
        tArea.setBounds(10,100,150,80);

        add(bLoghin);
        add(tArea);

    }

    public static void main(String[] args) {
        new Ex2();
    }

    class TratareButonLoghin implements ActionListener{
        private int counter = 0;
        public void actionPerformed(ActionEvent e) {

            counter++;
            Ex2.this.tArea.setText("");
            Ex2.this.tArea.append("Value:"+counter+"\n"); }
        }
    }
