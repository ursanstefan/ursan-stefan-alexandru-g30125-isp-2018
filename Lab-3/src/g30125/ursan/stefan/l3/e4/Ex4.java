package g30125.ursan.stefan.l3.e4;

import becker.robots.*;

public class Ex4
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Wall blockAve0 = new Wall(ny, 2, 2, Direction.EAST);
      Wall blockAve1 = new Wall(ny, 1, 2, Direction.EAST);
      Wall blockAve2 = new Wall(ny, 2, 2, Direction.SOUTH);
      Wall blockAve3 = new Wall(ny, 2, 1, Direction.SOUTH);
      Wall blockAve4 = new Wall(ny, 1, 2, Direction.NORTH);
      Wall blockAve5 = new Wall(ny, 1, 1, Direction.NORTH);
      Wall blockAve6 = new Wall(ny, 1, 1, Direction.WEST);
      Wall blockAve7 = new Wall(ny, 2, 1, Direction.WEST);
      Robot mark = new Robot(ny, 0, 2, Direction.WEST);
      //Robot ann = new Robot(ny, 0, 1, Direction.EAST);
 
 
      // mark goes around the roadblock
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
      mark.move();
      mark.move();
      mark.turnLeft();
      mark.move();
           
 
      // ann goes to meet mark
     
   }
} 