package g30125.ursan.stefan.l3.e6;

public class MyPoint {
	private int x;
	private int y;
	/*Constructor with no arguments*/
	public MyPoint()
	{
		x=0;
		y=0;	
	}
	
	/*Constructor with arguments*/
	public MyPoint(int cx, int cy)
	{
		x=cx;
		y=cy;
	}
	
	/* Getter And Setter */
	public int getX()
    {
        return x;
    }
    public int getY()
    {
        return y;
    }

    public void setX(int x)
    {
        this.x = x;
    }
    public void setY(int y)
    {
        this.y = y;
    }
    /*setXY*/
    public void setXY(int cx, int cy)
    {
    	this.x=cx;
    	this.y=cy;
    }
    
    /*Show XY*/
    void ShowXY() {
        System.out.println("("+x+","+y+")");
    }
    
    /* Distance between two points*/
    public double distance(int cx, int cy)
    {
    	return Math.sqrt((Math.pow(cx-x, 2)+Math.pow(cy-y, 2)));
    }
    
    /*Overloaded distance*/
    public double distance(MyPoint c)
    {
    	return Math.sqrt((Math.pow(c.x-x, 2)+Math.pow(c.y-y, 2)));
    }


}

