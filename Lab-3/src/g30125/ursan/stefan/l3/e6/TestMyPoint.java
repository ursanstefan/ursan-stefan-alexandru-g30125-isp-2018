package g30125.ursan.stefan.l3.e6;

public class TestMyPoint {
	 public static void main(String[] args) {
		 /*No argument constructor TEST*/
		 MyPoint point1 = new MyPoint();
		 MyPoint point2 = new MyPoint();
		 
		/*Argument Constructor TEST + Get X and get Y TEST*/
		 point1 = new MyPoint(1,1);
		 System.out.println("Arg.Constructor: "+point1.getX()+","+point1.getY());
		 
		 /*Show Coordinates TEST*/
		 point1.ShowXY();
		 
		 /*Set Coordinates TEST*/
		 point1.setXY(3, 4);
		 point1.ShowXY();
		 
		 /*Set X or Y coordinate TEST*/
		 point2.setX(7);
		 point2.ShowXY();
		 point2.setY(6);
		 point2.ShowXY();
		 
		 /*Distance TEST*/
		 System.out.println("Distance between points is: "+point1.distance(3, 3));
		 
		 /*Overloaded Distance TEST*/
		 System.out.println("Distance between points is: "+point1.distance(point2));
	 }

}
