package g30125.ursan.stefan.l4.e3;

public class Circle {
		private double radius=1.0;
		private String color="red";
		
		public Circle()
		{
			radius=1.0;
			color="red";
		}
		public Circle(double rad)
		{
			radius=rad;
			color="red";
		}
		public double getArea()
		{
			return 3.14*radius*radius;
		}
		public double getRadius()
		{
			return radius;
		}
		public static void main(String[] args)
		{
			Circle cerc=new Circle(2.2);
			System.out.println(cerc.getRadius()+", "+ cerc.color);
			System.out.println(cerc.getArea());
		}
		
}
