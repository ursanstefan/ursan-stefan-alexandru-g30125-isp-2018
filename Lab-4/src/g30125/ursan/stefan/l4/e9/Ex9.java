package g30125.ursan.stefan.l4.e9;

import becker.robots.*;

public class Ex9
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Wall blockAve0 = new Wall(ny, 5, 1, Direction.WEST);
      Wall blockAve1 = new Wall(ny, 5, 0, Direction.EAST);
      Wall blockAve2 = new Wall(ny, 4, 0, Direction.EAST);
      Wall blockAve3 = new Wall(ny, 3, 0, Direction.EAST);
      Wall blockAve4 = new Wall(ny, 5, 2, Direction.EAST);
      Wall blockAve5 = new Wall(ny, 5, 2, Direction.SOUTH);
      Wall blockAve6 = new Wall(ny, 5, 1, Direction.SOUTH);
      Wall blockAve7 = new Wall(ny, 3, 1, Direction.NORTH);
      Robot kerel = new Robot(ny, 2, 1, Direction.NORTH);
 
 
     
      kerel.move();
      kerel.turnLeft();
      kerel.turnLeft();     
      kerel.turnLeft();
      kerel.move();
      kerel.turnLeft();     
      kerel.turnLeft();
      kerel.turnLeft();     
      kerel.move();
      kerel.turnLeft();     
      kerel.turnLeft();
      kerel.turnLeft();
      kerel.turnLeft();     
      kerel.turnLeft();
      kerel.turnLeft();
      kerel.turnLeft(); 
      kerel.turnLeft();
      kerel.turnLeft(); 
      kerel.turnLeft(); 
      kerel.turnLeft();
      kerel.turnLeft();
      kerel.move();
      kerel.move();
      kerel.move();
      kerel.turnLeft();
      kerel.turnLeft(); 
      
 
     
   }
} 
