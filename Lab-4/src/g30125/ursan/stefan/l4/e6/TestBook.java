package g30125.ursan.stefan.l4.e6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30125.ursan.stefan.l4.e4.Author;


public class TestBook {
	
	@Test
	public void testToString() {
		Author[] author = new Author[2];
		author[0]=new Author("author","author@yahoo.com",'F');
				
		author[1]=new Author("author1","author1@yahoo.com",'M');
		Book b = new Book("b",author,45,78);
		
		assertEquals("book name b by 2",b.toString());
		
	}
	
	@Test
	public void testPrintAuthors() {
		Author[] author = new Author[2];
		author[0]=new Author("author","author@yahoo.com",'F');
				
		author[1]=new Author("author1","author1@yahoo.com",'M');
		Book b1 = new Book("booktitle",author,45,78);
		b1.printAuthors();
		assertEquals("author",author[0].getName());
		assertEquals("author1",author[1].getName());
		
		
	}

}