package g30125.ursan.stefan.l4.e5;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30125.ursan.stefan.l4.e4.Author;

public class TestBook {

	@Test
	public void testToString() {
		Author author = new Author("Authoor","Authoor@yahoo.com",'M');
		Book book = new Book("Titlu",author,999.98,3);
		assertEquals("book name Titlu by Author Authoor ( M ) at email Authoor@yahoo.com",book.toString());
	}
	
	@Test
	public void testGet() {
		Author author = new Author("a2","a2@yahoo.com",'f');
		Book book = new Book("booktitle",author,999.98,3);
		assertEquals("booktitle",book.getName());
	}
}