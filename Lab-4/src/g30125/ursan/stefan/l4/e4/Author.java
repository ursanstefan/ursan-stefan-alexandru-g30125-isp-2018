package g30125.ursan.stefan.l4.e4;

public class Author {
	private String name,email;
	private char gender;
	public Author(String name, String email, char genre)
	{
		this.name=name;
		this.email=email;
		this.gender = genre;
	}
	public String getName()
    {
        return name;
    }
    public String getEmail()
    {
        return email;
    }
    public char getGender()
    {
        return gender;
    }
    public void setEmail(String email)
    {
    	this.email=email;
    }
    public String toString() {
		return "Author "+name+" ( "+gender+" ) at email "+email;
	}
}
