package g30125.ursan.stefan.l4.e4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAuthor {
	@Test
	public void shouldPrintString() {
		Author author = new Author("Ursan","UrsanStefan@email.com",'M');
		assertEquals("Author Ursan ( M ) at email UrsanStefan@email.com",author.toString());
	}
}

