package g30125.ursan.stefan.l2.e6;

class Calculation
{
    int fact(int n)
    {
        int result;

       if(n==1)
         return 1;

       result = fact(n-1) * n;
       return result;
    }
}

public class Ex6 {	public Ex6() {}
	
	public static void main(String[] args) { java.util.Scanner in = new java.util.Scanner(System.in);
    System.out.println("Press 'a' for 'recursive' method or 'b' for 'non recursive' method ");
    String ch = in.nextLine();
    System.out.println("Enter a number");
    int x = in.nextInt();
    int b=1;
    if (ch.equals("a"))
    {
    	Calculation obj_one = new Calculation();

        int a = obj_one.fact(x);
        System.out.println("The factorial of the number is : " + a);
    }
    else
    	if (ch.equals("b"))
    	{
    		for(int i=1;i<=x;i++)
    			b=b*i;
    		System.out.println("The factorial of the number is : " + b );
    	}
}
}
