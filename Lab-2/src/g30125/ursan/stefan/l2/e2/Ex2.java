package g30125.ursan.stefan.l2.e2;


public class Ex2 { public Ex2() {}
  
  public static void main(String[] args) { java.util.Scanner in = new java.util.Scanner(System.in);
    System.out.println("Press 'a' for 'nested if' method or 'b' for 'switch' method ");
    String ch = in.nextLine();
    if (ch.equals("a"))
    {
      System.out.println("Enter a number");
      int x = in.nextInt();
      if (x == 1) {
        System.out.println("ONE");
      }
      else if (x == 2) {
        System.out.println("TWO");
      }
      else if (x == 3) {
        System.out.println("THREE");
      }
      else if (x == 4) {
        System.out.println("FOUR");
      }
      else if (x == 5) {
        System.out.println("FIVE");
      }
      else if (x == 6) {
        System.out.println("SIX");
      }
      else if (x == 7) {
        System.out.println("SEVEN");
      }
      else if (x == 8) {
        System.out.println("EIGHT");
      }
      else if (x == 9) {
        System.out.println("NINE");
      } else {
        System.out.println("OTHER");
      }
    }
    else if (ch.equals("b")) {
      System.out.println("Enter a number");
      int x = in.nextInt();
      switch (x) {
      case 1:  System.out.println("ONE");
        break;
      case 2:  System.out.println("TWO");
        break;
      case 3:  System.out.println("THREE");
        break;
      case 4:  System.out.println("FOUR");
        break;
      case 5:  System.out.println("FIVE");
        break;
      case 6:  System.out.println("SIX");
        break;
      case 7:  System.out.println("SEVEN");
        break;
      case 8:  System.out.println("EIGHT");
        break;
      case 9:  System.out.println("NINE");
        break;
      default:  System.out.println("OTHER");
      }
    }
  }
}
