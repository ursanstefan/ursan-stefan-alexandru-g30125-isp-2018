package g30125.ursan.stefan.l2.e5;

import java.util.Random;

public class Ex5 {
  public Ex5() {}
  
  public static void main(String[] args) { java.util.Scanner in = new java.util.Scanner(System.in);
    Random r = new Random();
    int[] x = new int[10];
    for (int i = 0; i < 10; i++)
    {
      x[i] = r.nextInt(50);
      System.out.println(x[i]);
    }
    
    for (int i = 0; i < 10; i++) {
      for (int j = 1; j < 10 - i; j++) {
        if (x[(j - 1)] > x[j])
        {
          int aux = x[(j - 1)];
          x[(j - 1)] = x[j];
          x[j] = aux;
        }
      }
    }
    System.out.println("vector sorted:");
    for (int i = 0; i < 10; i++) {
      System.out.println(x[i]);
    }
  }
}
