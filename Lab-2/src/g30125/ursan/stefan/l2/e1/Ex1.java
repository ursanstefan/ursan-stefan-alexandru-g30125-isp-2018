package g30125.ursan.stefan.l2.e1;

import java.util.Scanner;

public class Ex1 { public Ex1() {}
  
  public static void main(String[] args) { Scanner in = new Scanner(System.in);
  	System.out.println("X:");
    int x = in.nextInt();
    System.out.println("Y:");
    int y = in.nextInt();
    
    if (x == y) {
      System.out.println("X=Y");

    }
    else if (x > y) {
      System.out.println("X > Y");
    } else {
      System.out.println("Y<X");
    }
  }
}
