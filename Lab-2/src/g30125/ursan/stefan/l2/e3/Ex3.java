package g30125.ursan.stefan.l2.e3;

import java.util.Scanner;

public class Ex3 { public Ex3() {}
  
  public static void main(String[] args) { Scanner in = new Scanner(System.in);
    System.out.println("Enter two number");
    int A = in.nextInt();
    int B = in.nextInt();
    int prim = 1;int k = 0;
    for (int i = A; i <= B; i++)
    {
      prim = 1;
      for (int j = 2; j <= i / 2; j++)
        if (i % j == 0)
          prim = 0;
      if ((prim == 1) && (i != 1)) {
        k++;
        System.out.println(i);
      }
    }
    System.out.println(k + " prime numbers");
  }
}
