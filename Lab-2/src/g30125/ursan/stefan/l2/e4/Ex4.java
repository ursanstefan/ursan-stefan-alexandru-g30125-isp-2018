package g30125.ursan.stefan.l2.e4;

import java.util.Scanner;

public class Ex4 { public Ex4() {}
  
  public static void main(String[] args) { Scanner in = new Scanner(System.in);
    System.out.println("Enter the number of elements n=");
    int n = in.nextInt();
    int[] x = new int[n + 1];
    int max = 0;
    for (int i = 1; i <= n; i++)
    {
      System.out.println("Enter the number for " + i + " element");
      x[i] = in.nextInt();
      if (max < x[i])
        max = x[i];
    }
    System.out.println("The maximum element is: " + max);
  }
}
