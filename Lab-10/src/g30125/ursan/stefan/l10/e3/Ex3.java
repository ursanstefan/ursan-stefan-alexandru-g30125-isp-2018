package g30125.ursan.stefan.l10.e3;

class Ex3 extends Thread
{	int k1=0,k2=10;
      String n;
      Thread t;
      Ex3(String n, Thread t){this.n = n;this.t=t;}
 
      public void run()
      {
            System.out.println("Firul "+n+" a intrat in metoda run()");
            try
            {                
                  if (t!=null) { k1=10;k2=20;t.join();}
                  System.out.println("Firul "+n+" executa operatie.");
                  for(int i=k1;i<=k2;i++){
                      System.out.println(getName() + " i = "+i);}
                  Thread.sleep(300);
                  System.out.println("Firul "+n+" a terminat operatia.");
            }
            catch(Exception e){e.printStackTrace();} 
 
      }
 
public static void main(String[] args)
{
      Ex3 w1 = new Ex3("Counter 1",null);
      Ex3 w2 = new Ex3("Counter 2",w1);
      w1.run();
      w2.run();
}
}